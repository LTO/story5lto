from django.db import models

class Jadwal(models.Model):
    namaKegiatan = models.CharField(max_length=255)
    tanggal = models.DateField()
    jam = models.TimeField()
    tempat = models.CharField(max_length=255)
    kategori = models.CharField(max_length=255)


    def __str__(self):
        return self.namaKegiatan    

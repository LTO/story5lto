from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'story5ltoApp'

urlpatterns = [
    path('', views.index, name='Homepage'),
    path('profil/', views.profil, name='profil'),
    path('keahlian/', views.Keahlian, name='Keahlian'),
    path('pengalaman/', views.Pengalaman, name='Pengalaman'),
    path('kontak/', views.Kontak, name='Kontak'),
    path('pageTambahan/', views.pageTambahan, name='pageTambahan'),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('tambah/', views.tambahJadwal, name='tambah'),
    path('hapusJadwal/<jadwal_id>', views.hapusJadwal, name='hapus'),
]

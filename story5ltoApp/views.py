from django.shortcuts import render, redirect

from django.views.decorators.http import require_POST

from .models import Jadwal
from .forms import JadwalForm

def index(request):
    return render(request, 'Homepage.html')

def profil(request):
    return render(request, 'Profil.html')

def Keahlian(request):
    return render(request, 'Keahlian.html')

def Pengalaman(request):
    return render(request, 'Pengalaman.html')

def Kontak(request):
    return render(request, 'Kontak.html')

def pageTambahan(request):
    return render(request, 'pageTambahan.html')

def jadwal(request):
    list_jadwal = Jadwal.objects.order_by('tanggal')
    form = JadwalForm()
    context = {'list_jadwal' : list_jadwal, 'form' : form}
    return render(request, 'jadwal.html', context)

@require_POST
def tambahJadwal(request):
    form = JadwalForm(request.POST)

    jadwalBaru = Jadwal(namaKegiatan = request.POST['namaKegiatan'], tanggal = request.POST['tanggal'], jam = request.POST['jam'], tempat = request.POST['tempat'], kategori = request.POST['kategori'])
    jadwalBaru.save()
    
    return redirect('story5ltoApp:jadwal')


def hapusJadwal(request, jadwal_id):
    
    Jadwal.objects.filter(id=jadwal_id).delete()

    return redirect('story5ltoApp:jadwal')



# Create your views here.

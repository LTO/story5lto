from django import forms

class JadwalForm(forms.Form):
    namaKegiatan = forms.CharField(max_length=255, widget = forms.TextInput(attrs={'style' : 'font-size: 1vw', 'class' : 'form-control', 'placeholder' : 'Kegiatan', 'id' : 'namaKegiatan'}))
    tanggal = forms.DateField(widget = forms.DateInput(attrs={'style' : 'font-size: 1vw','class' : 'form-control', 'id' : 'tanggal', 'type' : 'date'}))
    jam = forms.TimeField(widget = forms.TimeInput(attrs={'style' : 'font-size: 1vw','class' : 'form-control', 'id' : 'jam', 'type' : 'time'}))
    tempat = forms.CharField(max_length=255, widget = forms.TextInput(attrs={'style' : 'font-size: 1vw','class' : 'form-control', 'placeholder' : 'Tempat', 'id' : 'tempat'}))
    kategori = forms.CharField(max_length=255, widget = forms.TextInput(attrs={'style' : 'font-size: 1vw','class' : 'form-control', 'placeholder' : 'Ketegori kegiatan', 'id' : 'kategori'}))